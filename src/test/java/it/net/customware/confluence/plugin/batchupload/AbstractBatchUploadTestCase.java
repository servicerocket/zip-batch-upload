package it.net.customware.confluence.plugin.batchupload;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;

import java.io.File;
import java.io.IOException;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipEntry;
import java.util.Collection;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;

public abstract class AbstractBatchUploadTestCase extends AbstractConfluencePluginWebTestCase
{
    private static final Logger LOGGER = Logger.getLogger(AbstractBatchUploadTestCase.class);

    protected File createUniqueTempFile(final String prefix, final String suffix) throws IOException
    {
        return File.createTempFile(prefix, suffix);
    }

    protected byte[] generateRandomContent() throws UnsupportedEncodingException
    {
        return RandomStringUtils.randomAlphanumeric(32).getBytes("UTF-8");
    }

    protected void removeFiles(final Collection<File> files)
    {
        if (null != files)
        {
            for (final File file : files)
            {
                try
                {
                    LOGGER.debug("Removing file: " + file);
                    if (file.isDirectory())
                        FileUtils.deleteDirectory(file);
                    else if (file.isFile())
                        FileUtils.deleteQuietly(file);
                }
                catch (final IOException ioe)
                {
                    /* Swallow */
                }
            }
        }
    }

    protected File createFileInTempDir(final String entry, final byte[] content) throws IOException
    {
        OutputStream out = null;

        try
        {
            final File theFile = new File(SystemUtils.getJavaIoTmpDir(), entry);
            final File theFileParent = theFile.getParentFile();

            if (!theFileParent.exists())
                assertTrue(theFileParent.mkdirs());

            out = new BufferedOutputStream(new FileOutputStream(theFile));
            out.write(content);
            out.flush();

            return theFile;
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }


    protected Collection<File> createTestZip(final File destinationZipFile, final String ... entries) throws IOException
    {
        ZipOutputStream out = null;

        try
        {
            final Collection<File> zipEntriesCreated = new ArrayList<File>();
            out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(destinationZipFile)));

            for (final String entry : entries)
            {
                InputStream in = null;
                try
                {
                    final ZipEntry zipEntry = new ZipEntry(entry);
                    final File entryFile = createFileInTempDir(entry, generateRandomContent());
                    
                    zipEntriesCreated.add(entryFile);
                    out.putNextEntry(zipEntry);

                    in = new BufferedInputStream(new FileInputStream(entryFile));
                    IOUtils.copy(in, out);
                }
                finally
                {
                    IOUtils.closeQuietly(in);
                    out.closeEntry();
                }
            }

            out.finish();
            out.flush();

            return zipEntriesCreated;
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    protected void createSpace(final String spaceKey)
    {
        final SpaceHelper spaceHelper = getSpaceHelper();

        spaceHelper.setKey(spaceKey);
        spaceHelper.setName(spaceKey);
        assertTrue(spaceHelper.create());
    }
}
