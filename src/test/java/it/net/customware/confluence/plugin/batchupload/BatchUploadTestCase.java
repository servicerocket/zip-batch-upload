package it.net.customware.confluence.plugin.batchupload;

import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.io.FileUtils;

public class BatchUploadTestCase extends AbstractBatchUploadTestCase
{
    private String testSpaceKey;

    private Collection<File> filesToRemove;

    protected void setUp() throws Exception
    {
        super.setUp();
        testSpaceKey = "TST";
        createSpace(testSpaceKey);
    }

    protected void tearDown() throws Exception
    {
        removeFiles(filesToRemove);
        super.tearDown();
    }

    public void testUploadZipWithNormalFileAndComment() throws IOException
    {
        final PageHelper pageHelper = getPageHelper();
        final File zipFile = createUniqueTempFile("test", ".zip");
        final Collection<File> zipEntries = createTestZip(zipFile, "test.txt", "test2.txt");
        final File normalFile = createFileInTempDir("normal.txt", "normal.txt".getBytes("UTF-8"));
        final String normalFileComment = "Normal file comment.";

        filesToRemove = new ArrayList<File>(zipEntries);
        filesToRemove.addAll(Arrays.asList(zipFile, normalFile));

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testUploadZipWithNormalFileAndComment");
        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
        clickLinkWithText("Zip Upload");

        setWorkingForm("upload-attachments");
        setTextField("file_0", zipFile.getAbsolutePath());
        setTextField("file_1", normalFile.getAbsolutePath());
        setTextField("comment_1", normalFileComment);
        submit("confirm");


        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test.txt\"]");
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test2.txt\"]");
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"normal.txt\"]");

        assertEquals(
                normalFileComment,
                StringUtils.trim(
                        getElementTextByXPath(
                                "//table[@class='tableview attachments']//tr/td/a[. = \"normal.txt\"]/parent::td/parent::tr/td[@class='comment']"
                        )
                )
        );
    }

    public void testUploadZipTwiceCreatesAttachmentVersionHistory() throws IOException
    {
        final PageHelper pageHelper = getPageHelper();
        final File zipFile = createUniqueTempFile("test", ".zip");
        final Collection<File> zipEntries = createTestZip(zipFile, "test.txt", "test2.txt");
        final String contextPath = getConfluenceWebTester().getContextPath();

        filesToRemove = new ArrayList<File>(zipEntries);
        filesToRemove.add(zipFile);

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testUploadZipTwiceCreatesAttachmentVersionHistory");
        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
        clickLinkWithText("Zip Upload");

        setWorkingForm("upload-attachments");
        setTextField("file_0", zipFile.getAbsolutePath());
        submit("confirm");

        /* Re-upload */
        setWorkingForm("upload-attachments");
        setTextField("file_0", zipFile.getAbsolutePath());
        submit("confirm");

        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test.txt\"]");
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test.txt\"]/parent::td/parent::tr/td//img[@src='" + contextPath + "/images/icons/arrow_closed_active_16.gif']");
        
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test2.txt\"]");
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test2.txt\"]/parent::td/parent::tr/td//img[@src='" + contextPath + "/images/icons/arrow_closed_active_16.gif']");
    }

    public void testZipEntriesProcessedNotLimitedByMaxAttachmentsInUi() throws IOException
    {
        final PageHelper pageHelper = getPageHelper();
        final File zipFile = createUniqueTempFile("test", ".zip");
        /* By default, max file uploads in the Confluence UI is 5. */
        final Collection<File> zipEntries = createTestZip(zipFile, "test.txt", "test2.txt", "test3.txt", "test4.txt", "test5.txt", "test6.txt");

        filesToRemove = new ArrayList<File>(zipEntries);
        filesToRemove.add(zipFile);

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testUploadZipTwiceCreatesAttachmentVersionHistory");
        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
        clickLinkWithText("Zip Upload");

        setWorkingForm("upload-attachments");
        setTextField("file_0", zipFile.getAbsolutePath());
        submit("confirm");


        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test.txt\"]");
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test2.txt\"]");
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test3.txt\"]");
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test4.txt\"]");
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test5.txt\"]");
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test6.txt\"]");
    }

    public void testEntriesInDifferentDepthsOfZipFileWithSameBaseNameCausesError() throws IOException
    {
        final PageHelper pageHelper = getPageHelper();
        final File zipFile = createUniqueTempFile("test", ".zip");
        /* By default, max file uploads in the Confluence UI is 5. */
        final Collection<File> zipEntries = createTestZip(zipFile, "depth2/test.txt", "test.txt");

        filesToRemove = new ArrayList<File>(zipEntries);
        filesToRemove.add(zipFile);

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testEntriesInDifferentDepthsOfZipFileWithSameBaseNameCausesError");
        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
        clickLinkWithText("Zip Upload");

        setWorkingForm("upload-attachments");
        setTextField("file_0", zipFile.getAbsolutePath());
        submit("confirm");

        assertEquals(
                "Duplicates of " + zipFile.getName() + "!depth2/test.txt not added - " + zipFile.getName() + "!test.txt.",
                getElementTextByXPath("//div[@class='errorBox']/ul/li[1]")
        );
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test.txt\"]");
    }

    public void testEntriesInDifferentDepthsOfAnotherZipFileWithSameBaseNameCausesError() throws IOException
    {
        final PageHelper pageHelper = getPageHelper();
        final File zipFile = createUniqueTempFile("test", ".zip");
        final File otherZipFile = createUniqueTempFile("test", ".zip");
        /* By default, max file uploads in the Confluence UI is 5. */
        final Collection<File> zipEntries = createTestZip(zipFile, "depth2/test.txt", "test.txt");
        final Collection<File> otherZipEntries = createTestZip(otherZipFile, "depth2/test.txt", "test.txt");

        filesToRemove = new ArrayList<File>(zipEntries);
        filesToRemove.add(zipFile);
        filesToRemove.addAll(otherZipEntries);
        filesToRemove.add(otherZipFile);

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testEntriesInDifferentDepthsOfAnotherZipFileWithSameBaseNameCausesError");
        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
        clickLinkWithText("Zip Upload");

        setWorkingForm("upload-attachments");
        setTextField("file_0", zipFile.getAbsolutePath());
        setTextField("file_1", otherZipFile.getAbsolutePath());
        submit("confirm");

        assertEquals(
                "Duplicates of "
                        + zipFile.getName() 
                        + "!depth2/test.txt not added - "
                        + zipFile.getName() + "!test.txt, "
                        + otherZipFile.getName() + "!depth2/test.txt, "
                        + otherZipFile.getName() + "!test.txt.",
                getElementTextByXPath("//div[@class='errorBox']/ul/li[1]")
        );
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test.txt\"]");
    }

    public void testNonZipFileUploadWithSameBaseNameCausesError() throws IOException
    {
        final PageHelper pageHelper = getPageHelper();
        final File zipFile = createUniqueTempFile("test", ".zip");
        final File otherZipFile = createUniqueTempFile("test", ".zip");
        /* By default, max file uploads in the Confluence UI is 5. */
        final Collection<File> zipEntries = createTestZip(zipFile, "depth2/test.txt", "test.txt");
        final Collection<File> otherZipEntries = createTestZip(otherZipFile, "depth2/test.txt", "test.txt");
        final File nonZipFile = new File(SystemUtils.JAVA_IO_TMPDIR, "test.txt");

        FileUtils.writeStringToFile(nonZipFile, "foo");

        filesToRemove = new ArrayList<File>(zipEntries);
        filesToRemove.add(zipFile);
        filesToRemove.addAll(otherZipEntries);
        filesToRemove.add(otherZipFile);
        filesToRemove.add(nonZipFile);

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testNonZipFileUploadWithSameBaseNameCausesError");
        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
        clickLinkWithText("Zip Upload");

        setWorkingForm("upload-attachments");
        setTextField("file_0", zipFile.getAbsolutePath());
        setTextField("file_1", otherZipFile.getAbsolutePath());
        setTextField("file_2", nonZipFile.getAbsolutePath());
        submit("confirm");

        assertEquals(
                "Duplicates of "
                        + zipFile.getName()
                        + "!depth2/test.txt not added - "
                        + zipFile.getName() + "!test.txt, "
                        + otherZipFile.getName() + "!depth2/test.txt, "
                        + otherZipFile.getName() + "!test.txt, "
                        + nonZipFile.getName() + ".",
                getElementTextByXPath("//div[@class='errorBox']/ul/li[1]")
        );
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test.txt\"]");
    }

    public void testNonZipFileUploadAsFirstUploadWithSameBaseNameCausesError() throws IOException
    {
        final PageHelper pageHelper = getPageHelper();
        final File zipFile = createUniqueTempFile("test", ".zip");
        final File otherZipFile = createUniqueTempFile("test", ".zip");
        /* By default, max file uploads in the Confluence UI is 5. */
        final Collection<File> zipEntries = createTestZip(zipFile, "depth2/test.txt", "test.txt");
        final Collection<File> otherZipEntries = createTestZip(otherZipFile, "depth2/test.txt", "test.txt");
        final File nonZipFile = new File(SystemUtils.JAVA_IO_TMPDIR, "test.txt");

        FileUtils.writeStringToFile(nonZipFile, "foo");

        filesToRemove = new ArrayList<File>(zipEntries);
        filesToRemove.add(zipFile);
        filesToRemove.addAll(otherZipEntries);
        filesToRemove.add(otherZipFile);
        filesToRemove.add(nonZipFile);

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testNonZipFileUploadAsFirstUploadWithSameBaseNameCausesError");
        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
        clickLinkWithText("Zip Upload");

        setWorkingForm("upload-attachments");
        setTextField("file_0", nonZipFile.getAbsolutePath());
        setTextField("file_1", zipFile.getAbsolutePath());
        setTextField("file_2", otherZipFile.getAbsolutePath());
        submit("confirm");

        assertEquals(
                "Duplicates of "
                        + nonZipFile.getName() + " not added - "
                        + zipFile.getName() + "!depth2/test.txt, "
                        + zipFile.getName() + "!test.txt, "
                        + otherZipFile.getName() + "!depth2/test.txt, "
                        + otherZipFile.getName() + "!test.txt.",
                getElementTextByXPath("//div[@class='errorBox']/ul/li[1]")
        );
        assertElementPresentByXPath("//table[@class='tableview attachments']//tr/td/a[. = \"test.txt\"]");
    }
}
