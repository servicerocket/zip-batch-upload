package net.customware.confluence.plugin.batchupload.pages.actions;

import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.opensymphony.xwork.Action;
import junit.framework.TestCase;
import net.customware.confluence.plugin.batchupload.pages.BatchUploadManager;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.MockitoAnnotations.Mock;

public class BatchUploadActionTestCase extends TestCase
{

    @Mock private SettingsManager settingsManager;

    @Mock private BatchUploadManager batchUploadManager;

    private BatchUploadAction batchUploadAction;

    private Settings globalSettings;

    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        globalSettings = new Settings();
        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        batchUploadAction = new TestBatchUploadAction();
    }

    @Override
    protected void tearDown() throws Exception
    {
        settingsManager = null;
        batchUploadManager = null;
        super.tearDown();
    }

    public void testMaxAttachmentsSetToConfiguredMaxAttachmentsAfterDoDefault()
            throws Exception
    {
        globalSettings.setMaxAttachmentsInUI(2);

        assertEquals(Action.INPUT, batchUploadAction.doDefault());
        assertEquals(globalSettings.getMaxAttachmentsInUI(), batchUploadAction.getMaxAttachments());
    }

    private class TestBatchUploadAction extends BatchUploadAction
    {
        private TestBatchUploadAction()
        {
            setSettingsManager(BatchUploadActionTestCase.this.settingsManager);
            setBatchUploadManager(BatchUploadActionTestCase.this.batchUploadManager);
        }
    }
}
