package net.customware.confluence.plugin.batchupload.pages;

import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.util.GeneralUtil;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import net.customware.confluence.plugin.batchupload.pages.actions.MultiPartRequestWrapperWrapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Comparator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DefaultBatchUploadManager implements BatchUploadManager
{
    private static final String FILE_FIELD_NAME_PREFIX = "file_";

    private static final String COMMENT_FIELD_NAME_PREFIX = "comment_";
	
	private BootstrapManager bootstrapManager;

    private SettingsManager settingsManager;

    public void setSettingsManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }

    public void setBootstrapManager(BootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }
	
	private File getTempDirectory()
    {
		return new File(bootstrapManager.getFilePathProperty(BootstrapManager.TEMP_DIR_PROP));
	}
	
	private File getUniqueChildren(final File parent)
    {
        File uniqueFile;
		
		do {
			uniqueFile = new File(parent, RandomStringUtils.randomAlphanumeric(64));
		} while (uniqueFile.exists());
		
		return uniqueFile;
	}

    private boolean isZipFile(final String fileName)
    {
        return StringUtils.defaultString(StringUtils.lowerCase(fileName)).endsWith(".zip");
    }

    private File copyZipEntry(ZipInputStream zipInputStream, File tempDir)
            throws IOException
    {
        OutputStream out = null;

        try
        {
            final File zipEntryFile = getUniqueChildren(tempDir);

            out = new BufferedOutputStream(new FileOutputStream(zipEntryFile));
            IOUtils.copy(zipInputStream, out);

            return zipEntryFile;
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    private String getZipEntryBaseName(final ZipEntry zipEntry)
    {
        final List<String> zipNameComponents = Arrays.asList(StringUtils.split(zipEntry.getName(), "/"));
        return zipNameComponents.get(zipNameComponents.size() - 1);
    }

    private Collection<BatchEntry> getBatchEntriesFromZip(
            final File zipFile,
            final String zipFileNiceName,
            final Collection<File> resourcesCreatedStore) throws IOException
    {
        ZipInputStream zipInputStream = null;

        try
        {
            final File tempDir = getUniqueChildren(getTempDirectory());
            final Collection<BatchEntry> batchEntries = new ArrayList<BatchEntry>();
            final StringBuffer stringBuffer = new StringBuffer();
            ZipEntry zipEntry;

            if (!tempDir.mkdirs())
                throw new IOException("Unable to create directory: " + tempDir);

            resourcesCreatedStore.add(tempDir);

            zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)));

            while (null != (zipEntry = zipInputStream.getNextEntry()))
            {
                if (!zipEntry.isDirectory())
                {
                    stringBuffer.setLength(0);

                    final BatchEntry batchEntry = new BatchEntry(
                            getZipEntryBaseName(zipEntry),
                            stringBuffer.append(zipFileNiceName).append('!').append(zipEntry.getName()).toString(),
                            copyZipEntry(zipInputStream, tempDir),
                            StringUtils.defaultString(zipEntry.getComment())
                    );

                    batchEntries.add(batchEntry);
                }
            }

            return batchEntries;
        }
        finally
        {
            IOUtils.closeQuietly(zipInputStream);
        }
    }

    private boolean isFileParam(final String paramName)
    {
        return paramName.matches("file_\\d+");
    }

    private Collection<String> getFileParams(final MultiPartRequestWrapper multiPartRequestWrapper)
    {
        final Collection<String> filteredFileParams = new TreeSet<String>(
                new Comparator<String>()
                {
                    private int getFileParamIndex(final String fileParam)
                    {
                        return Integer.parseInt(fileParam.substring(fileParam.indexOf('_') + 1));
                    }

                    public int compare(String fileParamLeft, String fileParamRight)
                    {
                        return getFileParamIndex(fileParamLeft) - getFileParamIndex(fileParamRight);
                    }
                }
        );
        final Enumeration fileParams = multiPartRequestWrapper.getFileParameterNames();

        while (fileParams.hasMoreElements())
        {
            final String fileParam = (String) fileParams.nextElement();

            if (isFileParam(fileParam))
                filteredFileParams.add(fileParam);
        }

        return filteredFileParams;
    }

    private String getCommentParamFromFileParam(String fileParam)
    {
        return fileParam.replaceAll("(file)(_\\d+)", "comment$2");
    }

    private boolean isDuplicateEntry(final Collection<BatchEntry> entries, final BatchEntry entry)
    {
        return entries.contains(entry);
    }

    private void addEntryAsDuplicate(
            final Map<BatchEntry, Collection<String>> duplicateEntries,
            final BatchEntry duplicatedEntry,
            final BatchEntry duplicateEntry)
    {
        Collection<String> names = duplicateEntries.get(duplicatedEntry);

        if (null == names)
            names = new LinkedHashSet<String>();

        names.add(duplicateEntry.getName());
        duplicateEntries.put(duplicatedEntry, names);
    }

    private BatchEntry getDuplicatedEntry(final Collection<BatchEntry> entries, final BatchEntry entry)
    {
        for (final BatchEntry _entry : entries)
            if (_entry.equals(entry))
                return _entry;

        return null;
    }

    public MultiPartRequestWrapperWrapper getMultiPartRequestWrapper(final MultiPartRequestWrapper multiPartRequestWrapper) throws IOException
    {
        final Map<String, BatchEntry> fieldNamesToFilesMap = new HashMap<String, BatchEntry>();
        final Map<String, String[]> fileParamComments = new HashMap<String, String[]>();
        final Collection<File> extraResourcesCreated = new HashSet<File>();
        final Collection<BatchEntry> allEntries = new LinkedHashSet<BatchEntry>();
        final Map<BatchEntry, Collection<String>> duplicatedEntryMessagesMap = new LinkedHashMap<BatchEntry, Collection<String>>();

        for (final String fileParam : getFileParams(multiPartRequestWrapper))
        {
			final File[] files = multiPartRequestWrapper.getFiles(fileParam);
            final String[] fileNames = multiPartRequestWrapper.getFileNames(fileParam);

            if (null != files)
            {
                for (int index = 0; index < files.length; ++index)
                {
                    final File file = files[index];
                    final String fileName = fileNames[index];

                    if (null != file)
                    {
                        if (isZipFile(fileNames[index]))
                        {
                            final Collection<BatchEntry> batchEntriesFromZip = getBatchEntriesFromZip(file, fileName, extraResourcesCreated);

                            for (final BatchEntry entry : batchEntriesFromZip)
                            {
                                if (isDuplicateEntry(allEntries, entry))
                                    addEntryAsDuplicate(duplicatedEntryMessagesMap, getDuplicatedEntry(allEntries, entry), entry);
                                else
                                    allEntries.add(entry);
                            }
                        }
                        else
                        {
                            final String commentParam = getCommentParamFromFileParam(fileParam);
                            final String comment = StringUtils.defaultString(multiPartRequestWrapper.getParameter(commentParam));
                            final BatchEntry entry = new BatchEntry(
                                    multiPartRequestWrapper.getFileNames(fileParam)[0],
                                    fileName,
                                    file,
                                    comment);

                            if (isDuplicateEntry(allEntries, entry))
                                addEntryAsDuplicate(duplicatedEntryMessagesMap, getDuplicatedEntry(allEntries, entry), entry);
                            else
                                allEntries.add(entry);
                        }
                    }
                }
            }
        }

        final StringBuffer stringBuffer = new StringBuffer();
        int batchEntryIndex = 0;
        
        for (final BatchEntry batchEntry : allEntries)
        {
            stringBuffer.setLength(0);
            fieldNamesToFilesMap.put(
                    stringBuffer.append(FILE_FIELD_NAME_PREFIX).append(batchEntryIndex).toString(),
                    batchEntry);


            stringBuffer.setLength(0);
            fileParamComments.put(
                    stringBuffer.append(COMMENT_FIELD_NAME_PREFIX).append(batchEntryIndex).toString(),
                    new String[] { batchEntry.getComment() });

            ++batchEntryIndex;
        }

        final MultiPartRequestWrapperWrapper _multiPartRequestWrapper =  new MultiPartRequestWrapperWrapper(
                (HttpServletRequest) multiPartRequestWrapper.getRequest(),
                bootstrapManager,
                settingsManager,
                fieldNamesToFilesMap,
                fileParamComments,
                extraResourcesCreated
        );

        if (!duplicatedEntryMessagesMap.isEmpty())
        {
            stringBuffer.setLength(0);
            for (Map.Entry<BatchEntry, Collection<String>> entry : duplicatedEntryMessagesMap.entrySet())
            {
                for (final String path : entry.getValue())
                {
                    if (stringBuffer.length() > 0)
                        stringBuffer.append(", ");
                    stringBuffer.append(GeneralUtil.htmlEncode(path));
                }

                _multiPartRequestWrapper.addWarning(
                        ConfluenceActionSupport.getTextStatic(
                                "batchupload.error.duplicate",
                                Arrays.asList(
                                        GeneralUtil.htmlEncode(entry.getKey().getName()),
                                        stringBuffer.toString()
                                ).toArray()
                        )
                );
            }
        }

        return _multiPartRequestWrapper;
    }
}