package net.customware.confluence.plugin.batchupload.pages;

import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;

import java.io.IOException;

import net.customware.confluence.plugin.batchupload.pages.actions.MultiPartRequestWrapperWrapper;

public interface BatchUploadManager
{
    MultiPartRequestWrapperWrapper getMultiPartRequestWrapper(final MultiPartRequestWrapper multiPartRequestWrapper) throws IOException;
}