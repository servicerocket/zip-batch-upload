package net.customware.confluence.plugin.batchupload.pages;

import java.io.File;

public class BatchEntry
{
    private final String baseName;

    private final String name;

    private final File file;

    private final String comment;

    public BatchEntry(final String baseName, final String name, final File file, final String comment)
    {
        this.baseName = baseName;
        this.name = name;
        this.file = file;
        this.comment = comment;
    }

    public String getBaseName()
    {
        return baseName;
    }

    public String getName()
    {
        return name;
    }

    public File getFile()
    {
        return file;
    }

    public String getComment()
    {
        return comment;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BatchEntry batchEntry = (BatchEntry) o;

        if (baseName != null ? !baseName.equals(batchEntry.baseName) : batchEntry.baseName != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (baseName != null ? baseName.hashCode() : 0);
    }
}
