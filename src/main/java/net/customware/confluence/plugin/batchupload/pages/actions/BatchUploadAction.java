package net.customware.confluence.plugin.batchupload.pages.actions;

import com.atlassian.confluence.pages.actions.ViewPageAttachmentsAction;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import net.customware.confluence.plugin.batchupload.pages.BatchUploadManager;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class BatchUploadAction extends ViewPageAttachmentsAction {
	
	private BatchUploadManager batchUploadManager;

    private int maxAttachments;

    public void setBatchUploadManager(BatchUploadManager batchUploadManager) {
		this.batchUploadManager = batchUploadManager;
	}

    public int getMaxAttachments()
    {
        return maxAttachments;
    }

    private int getMaxAttachmentsInUi()
    {
        return settingsManager.getGlobalSettings().getMaxAttachmentsInUI();
    }

    public String doDefault() throws Exception
    {
        maxAttachments = getMaxAttachmentsInUi();
        return super.doDefault();
    }

    private void addMultipartRequestWarningsAsActionErrors(final MultiPartRequestWrapperWrapper multiPartRequestWrapperWrapper)
    {
        if (null != multiPartRequestWrapperWrapper)
            for (final String warning : multiPartRequestWrapperWrapper.getWarnings())
                addActionError(warning);
    }

    public String execute() throws Exception {
        final HttpServletRequest originalHttpServletRequest = getMultiPartRequest();
        MultiPartRequestWrapperWrapper multiPartRequestWrapper = null;

        try {
            multiPartRequestWrapper = wrapMultipartRequest(originalHttpServletRequest);
            maxAttachments = multiPartRequestWrapper.getTotalFilesInRequest();
            setServletRequest(multiPartRequestWrapper);

            return executeParent();
        }
        finally
        {
            addMultipartRequestWarningsAsActionErrors(multiPartRequestWrapper);
            maxAttachments = getMaxAttachmentsInUi();
            cleanUp(multiPartRequestWrapper);
            setServletRequest(originalHttpServletRequest);
        }
	}

    protected MultiPartRequestWrapperWrapper wrapMultipartRequest(HttpServletRequest originalHttpServletRequest)
            throws IOException
    {
        return batchUploadManager.getMultiPartRequestWrapper((MultiPartRequestWrapper) originalHttpServletRequest);
    }

    protected String executeParent()
            throws Exception
    {
        return super.execute();
    }

    private void cleanUp(MultiPartRequestWrapperWrapper multiPartRequestWrapper)
            throws IOException
    {
        if (null != multiPartRequestWrapper)
        {
            for (final File fileToRemove : multiPartRequestWrapper.getExtraResourcesCreated())
            {
                if (fileToRemove.isDirectory())
                    FileUtils.deleteDirectory(fileToRemove);
                if (fileToRemove.isFile())
                   fileToRemove.delete();
            }
        }
    }

    public void validate()
    {
        super.validate();
        final HttpServletRequest originalHttpServletRequest = ServletActionContext.getRequest();
        final MultiPartRequestWrapper multipartRequest;

        if (null != originalHttpServletRequest
				&& !MultiPartRequestWrapper.class.isAssignableFrom(originalHttpServletRequest.getClass())) {
			addActionError(getText("batchupload.error.notamultipart"));
        }

        multipartRequest = (MultiPartRequestWrapper) originalHttpServletRequest;
        if (null != multipartRequest && null == multipartRequest.getFileParameterNames()) {
            addActionError(getText("batchupload.error.maxsizeexceeded"));
        }
    }
}