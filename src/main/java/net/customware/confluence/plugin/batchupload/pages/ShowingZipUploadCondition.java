package net.customware.confluence.plugin.batchupload.pages;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.conditions.ShowingPageAttachmentsCondition;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.user.User;

public class ShowingZipUploadCondition extends ShowingPageAttachmentsCondition {

    private PermissionManager permissionManager;

    @Override
    public void setPermissionManager(PermissionManager permissionManager)
    {
        super.setPermissionManager(permissionManager);
        this.permissionManager = permissionManager; 
    }

    public boolean shouldDisplay(final WebInterfaceContext webInterfaceContext)
    {
        final User remoteUser = webInterfaceContext.getUser();

        return super.shouldDisplay(webInterfaceContext)
                && (permissionManager.isConfluenceAdministrator(remoteUser) || permissionManager.hasCreatePermission(remoteUser, webInterfaceContext.getPage(), Attachment.class));
    }
}