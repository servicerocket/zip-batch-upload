package net.customware.confluence.plugin.batchupload.pages.actions;

import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.renderer.util.FileTypeUtil;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import net.customware.confluence.plugin.batchupload.pages.BatchEntry;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/* This class is totally not unit testable (thanks to the parent class).. unless you're willing to maintain mountains of mock setups
   just for it. This means we will rely on func tests.
 */
///CLOVER:OFF
public class MultiPartRequestWrapperWrapper extends MultiPartRequestWrapper
{
    private final Map<String, BatchEntry> fieldNamesToFilesMap;

    private final Map<String, String[]> parameters;

    private final Collection<File> extraResourcesCreated;

    private final Collection<String> warningMessages;

    public MultiPartRequestWrapperWrapper(
            final HttpServletRequest httpServletRequest,
            final BootstrapManager bootstrapManager,
            final SettingsManager settingsManager,
            final Map<String, BatchEntry> fieldNamesToBatchEntriesMap,
            final Map<String, String[]> parameters,
            final Collection<File> extraResourcesCreated)
            throws IOException
    {
        super(httpServletRequest,
                bootstrapManager.getFilePathProperty(BootstrapManager.TEMP_DIR_PROP),
                (int) settingsManager.getGlobalSettings().getAttachmentMaxSize());
        this.fieldNamesToFilesMap = fieldNamesToBatchEntriesMap;
        this.parameters = parameters;
        this.extraResourcesCreated = extraResourcesCreated;
        warningMessages = new TreeSet<String>();
    }

    public int getTotalFilesInRequest()
    {
        return fieldNamesToFilesMap.size();
    }

    public Collection<File> getExtraResourcesCreated()
    {
        return extraResourcesCreated;
    }

    public Enumeration getFileNames()
    {
        return getFileParameterNames();
    }

    public Enumeration getFileParameterNames()
    {
        return Collections.enumeration(fieldNamesToFilesMap.keySet());
    }

    public String getContentType(final String name)
    {
        return FileTypeUtil.getContentType(fieldNamesToFilesMap.get(name).getBaseName());
    }

    public String[] getContentTypes(final String name)
    {
        return new String[] { FileTypeUtil.getContentType(fieldNamesToFilesMap.get(name).getBaseName()) };
    }

    public File getFile(final String fieldName)
    {
        final File[] files = getFiles(fieldName);
        return null != files && files.length > 0 ? files[0] : null;
    }

    public File[] getFiles(final String fieldName)
    {
        return fieldNamesToFilesMap.containsKey(fieldName)
                ? new File[] { fieldNamesToFilesMap.get(fieldName).getFile() }
                : null;
    }

    public String[] getFileNames(final String fieldName)
    {
        return fieldNamesToFilesMap.containsKey(fieldName)
                ? new String[] { fieldNamesToFilesMap.get(fieldName).getBaseName() }
                : null;
    }

    public String getFilesystemName(final String fieldName)
    {
        final String[] fileNames = getFileSystemNames(fieldName);
        return null != fileNames && fileNames.length > 0 ? fileNames[0] : null;
    }

    public String[] getFileSystemNames(final String fieldName)
    {
        return getFileNames(fieldName);
    }

    public String getParameter(String s)
    {
        final String[] paramValues = getParameterValues(s);
        return null != paramValues && paramValues.length > 0 ? paramValues[0] : null;
    }

    public Map getParameterMap()
    {
        return new HashMap<String, String[]>(parameters);
    }

    public Enumeration getParameterNames()
    {
        return Collections.enumeration(parameters.keySet());
    }

    public String[] getParameterValues(String s)
    {
        return parameters.containsKey(s) ? parameters.get(s) : null;
    }

    @Override
    public void addError(String s)
    {
        super.addError(s);
    }

    public void addWarning(String message)
    {
        warningMessages.add(message);
    }

    public boolean hasWarning()
    {
        return !warningMessages.isEmpty();
    }

    public Collection<String> getWarnings()
    {
        return new TreeSet<String>(warningMessages);
    }
}
///CLOVER:ON